# Pipelines images
This repository contains base images needed pipelines.

## Reason
The reason is to prevent rate limiting on Docker's end.

## Usage
Target the image by referring to the GitLab registry concatenated with the group and project name.

```yaml
image: registry.gitlab.com/datails/docker-images/base
```

### Images
There are 3 base images, each in a repository of the registry linked to this repository:

#### Base image
Contains all CLI's needed in the pipeline. Including: `aws`, `node`, `kubectl`, `docker`.

```yaml
image: registry.gitlab.com/datails/docker-images/base
```

#### Docker image
Contains Docker.

```yaml
image: registry.gitlab.com/datails/docker-images/docker
```

#### Dind image
Contains Docker-in-docker. Target by adding an alias, to link to the docker TCP network.

```yaml
image: 
    name: registry.gitlab.com/datails/docker-images/dind
    alias: docker
```